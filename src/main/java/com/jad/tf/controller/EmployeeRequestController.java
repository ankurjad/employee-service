package com.jad.tf.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.mapstruct.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jad.tf.model.AppEvents;
import com.jad.tf.model.EmployeeRequestVO;
import com.jad.tf.processor.RequestProcessor;
import com.jad.tf.processor.employee.EmployeeRequestProcessor;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@EnableAutoConfiguration
@RequestMapping("/jadservices/")
@CrossOrigin(maxAge = 3600)
@Api(value="employee", description="Operations pertaining to Employee") 
public class EmployeeRequestController {

	@Autowired
	EmployeeRequestProcessor  employeeRequestProcessor;
	
	@Autowired
	EmployeeRequestVO employeeRequestVO;
	
	@CrossOrigin("http://localhost:4200")
	@ApiOperation(value = "Create Employee", response = ResponseEntity.class)	
	@RequestMapping(value ="/employee", method = RequestMethod.POST) 
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}	)
	@ResponseBody
	ResponseEntity<?> createEmployee(@RequestBody Object body){
		
		employeeRequestVO.setBody(body);
		employeeRequestVO.setEvents(AppEvents.CREATE_EMPLOYEE);
		
		
		RequestProcessor requestProcessor = new RequestProcessor();
		requestProcessor.setProcessor(employeeRequestProcessor);
		
		return (ResponseEntity<?>) requestProcessor.processRequest(employeeRequestVO);		
		
	}
	
	@CrossOrigin("http://localhost:4200")
	@ApiOperation(value = "Get All Employees", response = ResponseEntity.class)	
	@RequestMapping(value ="/getAllEmployee", method = RequestMethod.GET) 	
	@ResponseBody
	ResponseEntity<?> getAllEmployee(){
		
		employeeRequestVO.setEvents(AppEvents.FECTH_ALL_EMPLOYEES);
		
		RequestProcessor requestProcessor = new RequestProcessor();
		requestProcessor.setProcessor(employeeRequestProcessor);
		
		return (ResponseEntity<?>) requestProcessor.processRequest(employeeRequestVO);		
		
	}

	@ApiOperation(hidden=true, value = "")
	@RequestMapping("*")
	@ResponseBody
	public ResponseEntity<?> fallbackMethod(){
		return new ResponseEntity(HttpStatus.BAD_REQUEST);
	}
}