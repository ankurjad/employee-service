package com.jad.tf.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope (value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EmployeeRequestVO extends BaseRequestVO{

	AppEvents events;

	public AppEvents getEvents() {
		return events;
	}

	public void setEvents(AppEvents events) {
		this.events = events;
	}
	
}
