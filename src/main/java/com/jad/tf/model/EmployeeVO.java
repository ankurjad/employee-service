package com.jad.tf.model;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
	"id",
    "firstname",
    "lastname",
    "gender",
    "dob",
    "department"
})
public class EmployeeVO {
	
	@JsonProperty("id")
	private String id;

	@JsonProperty("firstname")
	private String firstname;	
	
	@JsonProperty("lastname")
	private String lastname;
	
	@JsonProperty("gender")
	private String gender;
	
	@JsonProperty("dob")
	private String  dob;
	
	@JsonProperty("department")
	private String department;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstname;
	}

	public void setFirstName(String firstname) {
		this.firstname = firstname;
	}

	public String getLastName() {
		return lastname;
	}

	public void setLastName(String lastname) {
		this.lastname = lastname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	
}
