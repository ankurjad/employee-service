package com.jad.tf.model;

public class BaseRequestVO {
	
	private Object body;
	
	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}

}
