package com.jad.tf.processor.employee;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.jad.tf.entity.EmployeeEntity;
import com.jad.tf.model.EmployeeRequestVO;
import com.jad.tf.processor.IRetrieveRequest;
import com.jad.tf.repository.EmployeeRepository;

@Component
public class RetrieveEmployeeRequest implements IRetrieveRequest <EmployeeRequestVO, ResponseEntity<?>>{

	private static final Logger log = LoggerFactory.getLogger(RetrieveEmployeeRequest.class);
	
	@Autowired
	private EmployeeRepository repository;
	
	@Override
	public ResponseEntity<?> retrieveRequest(EmployeeRequestVO employeeRequestVO) 
	{
		
	   log.info("Retrieve all employee records");
	   
	   List<EmployeeEntity> employeeList = repository.findAll();
	   
	   return new ResponseEntity<>(employeeList, HttpStatus.OK);
	   
	   
	}

}
