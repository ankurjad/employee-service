package com.jad.tf.processor.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.jad.tf.model.EmployeeRequestVO;
import com.jad.tf.model.EmployeeVO;
import com.jad.tf.processor.IProcessRequest;
import com.jad.tf.util.Convertor;

@Component
public class ProcessEmployeeRequest implements IProcessRequest<EmployeeRequestVO, ResponseEntity<?>> {

	@Autowired 
	PersistEmployeeRequest persistRequest;	
	
	@Override
	public ResponseEntity<?> processRequest(EmployeeRequestVO employeeRequestVO) throws Exception 
	{
		
		EmployeeVO employee = (EmployeeVO) Convertor.jsonToVO(EmployeeVO.class, employeeRequestVO);
		
		return persistRequest.persistRequest(employee);		
		
	}


}
