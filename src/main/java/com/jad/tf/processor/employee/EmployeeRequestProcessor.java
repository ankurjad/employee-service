package com.jad.tf.processor.employee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.jad.tf.model.EmployeeRequestVO;
import com.jad.tf.processor.IProcessor;
import com.jad.tf.processor.Processor;

@Component
public class EmployeeRequestProcessor extends Processor implements IProcessor <EmployeeRequestVO,ResponseEntity<?>> {
	
	private static final Logger log = LoggerFactory.getLogger(EmployeeRequestProcessor.class);
	
	@Autowired
	ProcessEmployeeRequest processRequest;
	
	@Autowired
	RetrieveEmployeeRequest retrieveRequest;
	
	public EmployeeRequestProcessor(ProcessEmployeeRequest processRequest, RetrieveEmployeeRequest retrieveRequest)
	{		
		setProcessRequest(processRequest);
		setRetrieveRequest(retrieveRequest);
	}

	@Override
	public ResponseEntity<?> process(EmployeeRequestVO employeeRequestVO) 
	{	
		Object response = null;
		
		try
		{
			switch(employeeRequestVO.getEvents())
			{		
				case CREATE_EMPLOYEE:				
						
					response = getProcessRequest().processRequest(employeeRequestVO);	
					
				case FECTH_ALL_EMPLOYEES:
					
					response = getRetrieveRequest().retrieveRequest(employeeRequestVO);
			}
		}
		catch(Exception e)
		{
			log.error(e.toString());
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return (ResponseEntity<?>) response;
		
	}


}
