package com.jad.tf.processor.employee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.jad.tf.entity.EmployeeEntity;
import com.jad.tf.model.EmployeeVO;
import com.jad.tf.processor.IPersistRequest;
import com.jad.tf.repository.EmployeeRepository;

@Component
public class PersistEmployeeRequest implements IPersistRequest <EmployeeVO, ResponseEntity<?>>{

	private static final Logger log = LoggerFactory.getLogger(PersistEmployeeRequest.class);
	
	@Autowired
	private EmployeeRepository repository;
	
	@Override
	public ResponseEntity<?> persistRequest(EmployeeVO employeeVO) {
		
	   EmployeeEntity entity = new EmployeeEntity();
	   
	   entity.setFirstName(employeeVO.getFirstName());
	   entity.setLastName(employeeVO.getLastName());
	   entity.setGender(employeeVO.getGender());
	   entity.setDob(employeeVO.getDob());
	   entity.setDepartment(employeeVO.getDepartment());
	   
	   log.info("Persisting entity ");
	   repository.save(entity);
	   
	   log.info("Persisted entity id : " + entity.getId());
	   
       return new ResponseEntity<>(entity, HttpStatus.OK);
	   
	   
	}
	

}
