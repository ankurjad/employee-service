package com.jad.tf.processor;

public interface IPersistRequest <I,O> {
	
	O persistRequest(I I);

}
