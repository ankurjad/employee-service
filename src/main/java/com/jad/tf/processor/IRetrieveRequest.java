package com.jad.tf.processor;

public interface IRetrieveRequest <I,O> {
	
	O retrieveRequest(I I);

}
