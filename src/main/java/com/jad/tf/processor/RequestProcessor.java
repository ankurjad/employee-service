package com.jad.tf.processor;

public class RequestProcessor {
	
	
	IProcessor processor;	
	

	public void setProcessor(IProcessor processor) {
		this.processor = processor;
	}

	public Object processRequest(Object o)
	{
		return processor.process(o);
	}

	
}
