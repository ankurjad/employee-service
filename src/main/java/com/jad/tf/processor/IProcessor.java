package com.jad.tf.processor;

public interface IProcessor <I,O>{
	
	O process(I i);

}
