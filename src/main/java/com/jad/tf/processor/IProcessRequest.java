package com.jad.tf.processor;

public interface IProcessRequest <I,O> {

	public O processRequest(I i) throws Exception;
}
