package com.jad.tf.processor;

public class Processor {
	
	IPersistRequest  persistRequest;
	
	IRetrieveRequest retrieveRequest;
	
	IProcessRequest  processRequest;

	public IPersistRequest getPersistRequest() {
		return persistRequest;
	}

	public void setPersistRequest(IPersistRequest persistRequest) {
		this.persistRequest = persistRequest;
	}

	public IRetrieveRequest getRetrieveRequest() {
		return retrieveRequest;
	}

	public void setRetrieveRequest(IRetrieveRequest retrieveRequest) {
		this.retrieveRequest = retrieveRequest;
	}

	public IProcessRequest getProcessRequest() {
		return processRequest;
	}

	public void setProcessRequest(IProcessRequest processRequest) {
		this.processRequest = processRequest;
	}

}