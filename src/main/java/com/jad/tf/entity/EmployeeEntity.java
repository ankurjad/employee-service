package com.jad.tf.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "EMPLOYEE")
@NamedQuery(query = "select e from EmployeeEntity e", name = "query_find_all_emps")
public class EmployeeEntity {	
	
	public EmployeeEntity() 
	{
		
	}

	public EmployeeEntity(String firstname, String lastname, String gender,  String  dob, String department) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.gender = gender;
		this.dob = dob;
		this.department = department;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "EMP_ID", unique = true, nullable = false)
	private Long id;	
	
	@Column(name = "FIRST_NAME")
	private String firstname;	
	
	@Column(name = "LAST_NAME")
	private String lastname;
	
	@Column(name = "EMP_GENDER")
	private String gender;
	
	@Column(name = "EMP_DOB")
	private String  dob;
	
	@Column(name = "EMP_DEPTT")
	private String department;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	
	
	public String getFirstName() {
		return firstname;
	}

	public void setFirstName(String firstname) {
		this.firstname = firstname;
	}

	public String getLastName() {
		return lastname;
	}

	public void setLastName(String lastname) {
		this.lastname = lastname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

}
