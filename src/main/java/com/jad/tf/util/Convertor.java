package com.jad.tf.util;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jad.tf.model.BaseRequestVO;
import com.jad.tf.model.EmployeeVO;

public class Convertor {
	
	public static  Object jsonToVO(Class<?> clazz, BaseRequestVO baseRequestVO) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		
		//List<EmployeeVO> myObjects =
			//    mapper.readValue(baseRequestVO.getBody(), new TypeReference<List<EmployeeVO>>(){});
		
		return  mapper.convertValue(baseRequestVO.getBody(), clazz);
	}

}
