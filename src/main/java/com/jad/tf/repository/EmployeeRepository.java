package com.jad.tf.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jad.tf.entity.EmployeeEntity;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {
	   
}
