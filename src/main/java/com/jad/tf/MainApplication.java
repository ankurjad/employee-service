package com.jad.tf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

	 
@SpringBootApplication(scanBasePackages={"com.jad.tf"})
public class MainApplication {
	 
	public static void main(String[] args) {
	     SpringApplication.run(MainApplication.class, args);
	}
}